from flask import Flask
from flask import request
import json

app = Flask(__name__)

def inverse(calculatorAPI):
    def wrapper(*args, **kwargs):
        res = request.data
        res = json.loads(res)

        op1 = res.get("op1")
        op2 = res.get("op2")
        op = res.get("op")
        
        op = inverseSymbol(op)

        return calculatorAPI(op1, op2, op)
    return wrapper

@app.route("/calc", methods=['POST'])
@inverse
def calculatorAPI(op1, op2, op):
    if(op1 == None):
        op1 = 0
    if(op2 == None):
        op2 = 0
    
    if(op == "+"):
        return str(op1 + op2)
    elif(op == "-"):
        return str(op1 - op2)
    elif(op == "*"):
        return str(op1 * op2)
    elif(op == "/"):
        return str(op1 / op2)
    else:
        return f"Invalid Operator {op}"


# FUNCTIONS

def inverseSymbol(op):
    if(op == "+"):
        return "-"
    elif(op == "-"):
        return "+"
    elif(op == "*"):
        return "/"
    elif(op == "/"):
        return "*"